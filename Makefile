fbkeyboard: fbkeyboard.c
	gcc $$(pkg-config --cflags freetype2) -ansi fbkeyboard.c -o fbkeyboard $$(pkg-config --libs freetype2) $(CPPFLAGS) $(CFLAGS) $(LDFLAGS)

clean:
	rm -f fbkeyboard
